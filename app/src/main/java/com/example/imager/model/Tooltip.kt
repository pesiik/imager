package com.example.imager.model

import android.view.View

data class Tooltip (
    val targetView: View,
    val title: String,
    val description: String
)