package com.example.imager.model.entities

import androidx.room.*
import com.example.imager.model.ImageData
import com.example.imager.model.converters.ChangeTypeTypeConverter
import com.example.imager.model.converters.UuidTypeConverter
import com.example.imager.model.entities.ImageEntity.Companion.TABLE_NAME
import java.util.*

@Entity(
    tableName = TABLE_NAME,
    indices = [
        Index(
            ImageEntity.FINGERPRINT,
            unique = true
        ),
        Index(
            ImageEntity.PATH,
            unique = true
        ),
        Index(
            ImageEntity.POSITION,
            unique = true
        )
    ]
)
@TypeConverters(
    UuidTypeConverter::class,
    ChangeTypeTypeConverter::class
)
data class ImageEntity(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = POSITION) val position: Int?,
    @ColumnInfo(name = FINGERPRINT) val fingerprint: UUID,
    @ColumnInfo(name = PATH) val path: String,
    @ColumnInfo(name = CHANGE_TYPE) val changeType: ImageData.ChangeType
) {
    companion object {
        const val TABLE_NAME = "image_entity"

        const val FINGERPRINT = "fingerprint"
        const val PATH = "path"
        const val POSITION = "position"
        const val CHANGE_TYPE = "change_type"
    }
}