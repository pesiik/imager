package com.example.imager.model

import android.content.Context
import com.f2prateek.rx.preferences2.RxSharedPreferences

class Store(context: Context) {

    companion object {
        private const val SP_NAME = "imager"
        private const val MAIN_IMAGE = "main_image"
        private const val SHOULD_SHOW_GUIDE = "should_show_guide"
    }

    private val sp = context.getSharedPreferences(SP_NAME, Context.MODE_PRIVATE)
    private val rxPreferences = RxSharedPreferences.create(sp)
    val mainImagePath = rxPreferences.getString(MAIN_IMAGE, "")
    val shouldShowGuid = rxPreferences.getBoolean(SHOULD_SHOW_GUIDE, true)
}