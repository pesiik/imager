package com.example.imager.model.converters

import androidx.room.TypeConverter
import com.example.imager.model.ImageData
import java.lang.IllegalArgumentException

class ChangeTypeTypeConverter {
    @Suppress("unused")
    @TypeConverter
    fun toType(value: String): ImageData.ChangeType = when (value) {
        ImageData.ChangeType.Mirrored.name -> ImageData.ChangeType.Mirrored
        ImageData.ChangeType.Rotated.name -> ImageData.ChangeType.Rotated
        ImageData.ChangeType.BlackWhited.name -> ImageData.ChangeType.BlackWhited
        else -> throw IllegalArgumentException("Illegal ChangeType")
    }

    @Suppress("unused")
    @TypeConverter
    fun fromType(changeType: ImageData.ChangeType): String = changeType.name
}