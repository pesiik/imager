package com.example.imager.model.daos

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.imager.model.converters.ChangeTypeTypeConverter
import com.example.imager.model.converters.UuidTypeConverter
import com.example.imager.model.entities.ImageEntity
import com.example.imager.model.entities.ImageEntity.Companion.TABLE_NAME
import java.util.*

@Dao
interface ImageDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertImage(imageEntity: ImageEntity)

    @Query(
        """
            DELETE FROM image_entity WHERE fingerprint == :fingerprint
        """
    )
    @TypeConverters(
        UuidTypeConverter::class
    )
    fun deleteImageById(fingerprint: UUID)

    @Query(
        """
        SELECT *
        FROM $TABLE_NAME
        ORDER BY position ASC
    """
    )
    @TypeConverters(
        UuidTypeConverter::class,
        ChangeTypeTypeConverter::class
    )
    fun allImages(): LiveData<List<ImageEntity>>
}