package com.example.imager.model

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.imager.model.daos.ImageDao
import com.example.imager.model.entities.ImageEntity

@Database(
    entities = [
        ImageEntity::class
    ],
    version = 1,
    exportSchema = true
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun imageDao() : ImageDao
}