package com.example.imager.model

import com.example.imager.model.entities.ImageEntity

fun ImageEntity.toData() = ImageData(
    order = this.position,
    fingerprint = this.fingerprint,
    path = this.path,
    changeType = this.changeType
)

fun ImageData.toEntity() = ImageEntity(
    position = this.order,
    fingerprint = this.fingerprint,
    path = this.path,
    changeType = this.changeType
)