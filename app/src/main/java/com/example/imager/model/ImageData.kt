package com.example.imager.model

import java.util.*

data class ImageData(
    val fingerprint: UUID,
    val path: String,
    val changeType: ChangeType,
    val order: Int? = null
) {
    enum class ChangeType {
        Rotated,
        Mirrored,
        BlackWhited
    }
}