package com.example.imager.model

import android.content.Context
import androidx.room.Room

object DBStore {
    private const val DB_NAME = "imager.sqlite"

    lateinit var dataBase: AppDatabase

    fun init(applicationContext: Context){
        dataBase = Room.databaseBuilder(applicationContext, AppDatabase::class.java, DB_NAME).build()
    }

}