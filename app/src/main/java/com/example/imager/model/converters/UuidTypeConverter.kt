package com.example.imager.model.converters

import androidx.room.TypeConverter
import java.util.*

class UuidTypeConverter {

    @Suppress("unused")
    @TypeConverter
    fun toUuid(value: String?): UUID? {
        return value?.let {
            return UUID.fromString(it)
        }
    }

    @Suppress("unused")
    @TypeConverter
    fun fromUuid(uuid: UUID?): String? {
        return uuid?.toString()
    }

}