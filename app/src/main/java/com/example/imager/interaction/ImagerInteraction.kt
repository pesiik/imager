package com.example.imager.interaction

interface ImagerInteraction {
    fun onMonochrome()
    fun onMirror()
    fun onRotate()
}