package com.example.imager.interaction

import android.graphics.Bitmap
import android.widget.Toast
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import com.example.imager.*
import com.example.imager.model.ImageData
import com.example.imager.viewmodels.viewModelImages
import io.reactivex.disposables.Disposable
import java.util.*

class InteractionManager(
    private val activity: ImagerActivity
) : LifecycleObserver, ImagerInteraction {

    private val imagesViewModel by lazy { activity.viewModelImages() }
    private var disposable: Disposable? = null

    override fun onMonochrome() {
        processImage(ImageData.ChangeType.BlackWhited)
    }

    override fun onMirror() {
        processImage(ImageData.ChangeType.Mirrored)
    }

    override fun onRotate() {
        processImage(ImageData.ChangeType.Rotated)
    }


    private fun processImage(type: ImageData.ChangeType) {
        disposable = fromCallable(
            {
                activity.runOnUiThread {
                    activity.enableUI(false)
                    activity.enableProgressBar(true)
                }
                when (type) {
                    ImageData.ChangeType.Mirrored -> imagesViewModel.currentBitmap?.mirror()
                    ImageData.ChangeType.BlackWhited -> imagesViewModel.currentBitmap?.monochrome()
                    ImageData.ChangeType.Rotated -> imagesViewModel.currentBitmap?.rotate()
                }
            },
            {
                activity.runOnUiThread {
                    activity.enableUI(true)
                    activity.enableProgressBar(false)
                }
            },
            {
                activity.runOnUiThread {
                    Toast.makeText(activity, R.string.errorToast, Toast.LENGTH_LONG).show()
                }
            }
        )
            .subscribe { bitmap: Bitmap?, _: Throwable? ->
                saveChangedBitmap(bitmap, type)
            }
    }

    private fun saveChangedBitmap(bitmap: Bitmap?, type: ImageData.ChangeType) {
        val newFile = bitmap?.toFile(activity)
        newFile?.let {
            val imageData = ImageData(UUID.randomUUID(), it.absolutePath, type)
            imagesViewModel.addImage(imageData)
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun onDestroy() {
        disposable?.dispose()
    }
}