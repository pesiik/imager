package com.example.imager


import android.graphics.Bitmap
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import androidx.appcompat.app.AlertDialog
import androidx.core.content.FileProvider
import androidx.lifecycle.LiveDataReactiveStreams
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.imager.model.ImageData
import com.example.imager.model.Store
import com.example.imager.model.Tooltip
import com.example.imager.model.toData
import com.example.imager.viewmodels.viewModelImages
import com.squareup.picasso.Callback
import com.squareup.picasso.MemoryPolicy
import com.squareup.picasso.Picasso
import io.reactivex.BackpressureStrategy
import io.reactivex.subjects.PublishSubject
import java.io.File
import java.lang.Exception
import java.util.*

class ImagesListFragment : Fragment() {

    private val imagesViewModel by lazy { activity!!.viewModelImages() }

    private lateinit var imagesRecycler: RecyclerView
    private var imageAdapter: ImageAdapter? = null

    private val store by lazy { Store(activity!!.applicationContext) }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_images_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        imagesRecycler = view.findViewById(R.id.images_list)
        imagesRecycler.layoutManager = LinearLayoutManager(context)
        imageAdapter = ImageAdapter(LinkedList(emptyList<ImageData>()))
        imagesViewModel.allImages().observe(this, Observer { entities ->
            val images = entities.map {
                it.toData()
            }
            imageAdapter?.updateList(images)
            imagesRecycler.adapter = imageAdapter
            imagesRecycler.visibility = if (images.isNotEmpty()) View.VISIBLE else View.GONE
        })
        imageAdapter?.getPositionClicks()?.observe(this, Observer { selectedItem ->
            val file = File(selectedItem.path)
            val uri = FileProvider.getUriForFile(activity!!, ImagerActivity.FILE_PROVIDER, file)
            imagesViewModel.currentPath.postValue(uri)
        })
    }

    private inner class ImageAdapter(private var generalList: LinkedList<ImageData>) :
        RecyclerView.Adapter<ImageAdapter.ImageHolder>() {

        private val onClickSubject: PublishSubject<ImageData> = PublishSubject.create()

        inner class DiffUtilsCallback(private val oldList: List<ImageData>, private val newList: List<ImageData>) :
            DiffUtil.Callback() {

            override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) =
                oldList[oldItemPosition].fingerprint == newList[newItemPosition].fingerprint

            override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int) =
                oldList[oldItemPosition].path == newList[newItemPosition].path

            override fun getOldListSize() = oldList.size

            override fun getNewListSize() = newList.size
        }

        override fun onBindViewHolder(holder: ImageHolder, position: Int) {}

        override fun onBindViewHolder(holder: ImageHolder, position: Int, payload: List<Any>) {
            val item = generalList[position]
            holder.updateImage(item)
            holder.itemView.setOnClickListener {
                startActivity(FullScreenImageActivity.intent(activity!!, item.path))
            }
            holder.itemView.setOnLongClickListener {
                val dialog = AlertDialog.Builder(activity!!)
                    .setView(R.layout.dialog_image_action)
                    .create()
                dialog.show()
                dialog.findViewById<Button>(R.id.edit_button)!!.setOnClickListener {
                    onClickSubject.onNext(item)
                    dialog.dismiss()
                }
                dialog.findViewById<Button>(R.id.delete_button)!!.setOnClickListener {
                    imagesViewModel.deleteImage(item.fingerprint)
                    dialog.dismiss()
                }
                true
            }
        }

        fun updateList(list: List<ImageData>) {
            val diffUtil = DiffUtil.calculateDiff(DiffUtilsCallback(generalList, list), true)
            generalList.clear()
            generalList.addAll(list)
            imageAdapter?.let {
                diffUtil.dispatchUpdatesTo(it)
            }

        }

        fun getPositionClicks() =
            LiveDataReactiveStreams.fromPublisher<ImageData>(onClickSubject.toFlowable(BackpressureStrategy.BUFFER))

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageHolder {
            return ImageHolder(parent)
        }

        override fun getItemCount(): Int {
            return generalList.size
        }


        inner class ImageHolder(root: ViewGroup) : RecyclerView.ViewHolder(
            this@ImagesListFragment.layoutInflater.inflate(R.layout.image_item, root, false)
        ) {
            val changedImage: ImageView = itemView.findViewById(R.id.changed_image)
            val guideAnchor: View = itemView.findViewById(R.id.guide_anchor)

            fun updateImage(imageData: ImageData) {
                val file = File(imageData.path)
                Picasso.get().load(file).memoryPolicy(MemoryPolicy.NO_STORE).config(Bitmap.Config.RGB_565)
                    .into(changedImage, object : Callback {
                        override fun onSuccess() {
                            if (store.shouldShowGuid.get()) {
                                activity!!.showTooltips(
                                    Tooltip(
                                        guideAnchor,
                                        resources.getString(R.string.image_tooltip_title),
                                        resources.getString(R.string.image_tooltip_description)
                                    )
                                )
                                store.shouldShowGuid.set(false)
                            }
                        }

                        override fun onError(e: Exception?) {}
                    })
            }
        }
    }

    companion object {
        const val TAG = "ImagesListFragment"

        @JvmStatic
        fun newInstance() = ImagesListFragment()
    }
}
