package com.example.imager

import android.graphics.Bitmap
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


fun fromCallable(item: () -> Bitmap?, onFinally: () -> Unit, onError: ((Throwable) -> Unit)? = null) =
    Single.fromCallable(item)
        .doOnError {
            onError?.invoke(it)
        }
        .doFinally {
            onFinally()
        }
        .observeOn(AndroidSchedulers.mainThread())
        .subscribeOn(Schedulers.io())