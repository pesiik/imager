package com.example.imager

import android.app.Activity
import com.example.imager.model.Tooltip
import com.getkeepsafe.taptargetview.TapTarget
import com.getkeepsafe.taptargetview.TapTargetSequence
import com.getkeepsafe.taptargetview.TapTargetView

fun Activity.showTooltips(vararg tooltips: Tooltip) {
    if (tooltips.size == 1) {
        TapTargetView.showFor(this, tooltips[0].toTapTarget())
    } else {
        tooltips.map {
            it.toTapTarget()
        }.let {
            TapTargetSequence(this).targets(it).start()
        }
    }
}

private fun Tooltip.toTapTarget(): TapTarget =
    com.getkeepsafe.taptargetview.TapTarget.forView(
        targetView,
        title,
        description)
        .targetCircleColor(R.color.gray)
        .transparentTarget(true)
        .titleTextColor(R.color.white)
        .descriptionTextColor(R.color.white)
        .descriptionTextAlpha(1f)
        .outerCircleColor(R.color.black)
        .outerCircleAlpha(0.6f)
        .cancelable(true)
