package com.example.imager

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.os.Environment
import java.io.*
import java.text.SimpleDateFormat
import java.util.*
import android.graphics.Bitmap
import android.net.Uri
import androidx.core.content.FileProvider
import androidx.core.graphics.blue
import androidx.core.graphics.green
import androidx.core.graphics.red


fun Bitmap.rotate() = Bitmap.createBitmap(
    this,
    0,
    0,
    this.width,
    this.height,
    Matrix()
        .apply {
            postRotate(90f)
        },
    true
)

fun Bitmap.mirror() = Bitmap.createBitmap(
    this,
    0,
    0,
    this.width,
    this.height,
    Matrix()
        .apply {
            preScale(-1f, 1f)
        },
    true
)

fun Bitmap.monochrome(): Bitmap {
    val grayScale = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
    for (y in 0 until grayScale.height)
        for (x in 0 until grayScale.width) {
            val c = this.getPixel(x, y)
            val gs = (c.red * 0.3 + c.green * 0.59 + c.blue * 0.11).toInt()
            grayScale.setPixel(x, y, Color.argb(gs, gs, gs, gs))
        }
    return grayScale
}

@SuppressLint("SimpleDateFormat")
@Throws(IOException::class)
fun Context.createImageFile(): File {
    // Create an image file name
    val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
    val storageDir: File? = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
    return File.createTempFile(
        "JPEG_${timeStamp}_",
        ".jpg",
        storageDir
    )
}

fun Bitmap.toFile(context: Context): File {
    val file = context.createImageFile()
    val byteArrayOutputStream = ByteArrayOutputStream()
    compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream)
    val byteArray = byteArrayOutputStream.toByteArray()
    val fileOutputStream = FileOutputStream(file)
    fileOutputStream.write(byteArray)
    fileOutputStream.flush()
    fileOutputStream.close()
    return file
}

fun Context.saveFile(uri: Uri): Uri {
    val inputStream = contentResolver.openInputStream(uri)
    val file = createImageFile()
    val fileOutStream = FileOutputStream(file)
    copyStream(inputStream!!, fileOutStream)
    fileOutStream.close()
    inputStream.close()
    return FileProvider.getUriForFile(
        this,
        ImagerActivity.FILE_PROVIDER,
        file
    )
}

@Throws(IOException::class)
private fun copyStream(inputStream: InputStream, outputStream: OutputStream) {
    inputStream.use { input ->
        outputStream.use { output ->
            input.copyTo(output)
        }
    }
}

fun Bitmap.createLocalFile(context: Context): Uri {
    val photoFile = toFile(context)
    photoFile.also {
        return FileProvider.getUriForFile(
            context,
            ImagerActivity.FILE_PROVIDER,
            it
        )
    }
}