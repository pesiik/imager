package com.example.imager.fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button

import com.example.imager.R
import com.example.imager.interaction.ImagerInteraction
import com.example.imager.interaction.InteractionManager

class ImageInteractionFragment(
    private val imagerInteraction: ImagerInteraction
) : Fragment() {

    private lateinit var rotateButton: Button
    private lateinit var invertColorButton: Button
    private lateinit var mirrorButton: Button

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_image_interaction, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        rotateButton = view.findViewById(R.id.rotate_button)
        invertColorButton = view.findViewById(R.id.invert_color_button)
        mirrorButton = view.findViewById(R.id.mirror_button)
        initProcessButton()
    }

    fun enableUi(isEnabled: Boolean) {
        rotateButton.isEnabled = isEnabled
        mirrorButton.isEnabled = isEnabled
        invertColorButton.isEnabled = isEnabled
    }

    private fun initProcessButton() {
        mirrorButton.setOnClickListener {
            imagerInteraction.onMirror()
        }
        rotateButton.setOnClickListener {
            imagerInteraction.onRotate()
        }
        invertColorButton.setOnClickListener {
            imagerInteraction.onMonochrome()
        }
    }

    companion object {
        const val TAG = "ImageInteractionFragment"
        fun newInstance(interactionManager: InteractionManager) = ImageInteractionFragment(interactionManager)
    }
}
