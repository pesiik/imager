package com.example.imager

import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageButton
import android.widget.ImageView

class FullScreenImageActivity : AppCompatActivity() {

    companion object {
        const val EXTRA_URI = "EXTRA_URI"

        fun intent(context: Context, uri: String) = Intent(context, FullScreenImageActivity::class.java).apply {
            putExtra(EXTRA_URI, uri)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_full_size_image)
        findViewById<ImageView>(R.id.image).setImageURI(Uri.parse(intent.getStringExtra(EXTRA_URI)))
        findViewById<ImageButton>(R.id.close_image).setOnClickListener { finish() }
    }
}
