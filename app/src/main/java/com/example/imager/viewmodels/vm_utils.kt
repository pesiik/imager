package com.example.imager.viewmodels

import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProviders

fun FragmentActivity.viewModelImages() =
    ViewModelProviders.of(this).get(ImagesViewModel::class.java)
