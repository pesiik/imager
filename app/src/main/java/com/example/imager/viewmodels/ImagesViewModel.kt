package com.example.imager.viewmodels

import android.graphics.Bitmap
import android.net.Uri
import androidx.lifecycle.MutableLiveData
import com.example.imager.model.ImageData
import com.example.imager.model.DBStore
import com.example.imager.model.toEntity
import java.util.*

class ImagesViewModel : DisposablesViewModel() {

    private val db = DBStore.dataBase

    var currentPath: MutableLiveData<Uri> = MutableLiveData()
    var currentBitmap: Bitmap? = null

    var bufferPath: Uri? = null

    fun allImages() = db.imageDao().allImages()

    fun addImage(imageData: ImageData) = runOnIo {
        db.imageDao().insertImage(imageData.toEntity())
    }

    fun deleteImage(fingerprint: UUID) = runOnIo {
        db.imageDao().deleteImageById(fingerprint)
    }
}