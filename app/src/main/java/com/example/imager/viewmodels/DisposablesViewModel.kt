package com.example.imager.viewmodels

import androidx.lifecycle.ViewModel
import android.util.Log
import androidx.annotation.CallSuper
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers


abstract class DisposablesViewModel : ViewModel() {

    private val disposables = CompositeDisposable()

    protected fun <T> runOnIo(block: () -> T) {
        val disposable = Observable.fromCallable {
            block()
        }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({

            }, { th ->
                Log.w("DisposablesViewModel", "runOnIo onError", th)
            })
        disposables.add(disposable)
    }

    @CallSuper
    override fun onCleared() {
        super.onCleared()
        disposables.dispose()
    }

}
