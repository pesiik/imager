package com.example.imager

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.annotation.StringRes
import com.example.imager.fragments.ImageInteractionFragment
import com.example.imager.interaction.InteractionManager
import com.example.imager.model.DBStore
import com.example.imager.model.Store
import com.example.imager.viewmodels.viewModelImages
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import java.lang.Exception

class ImagerActivity : AppCompatActivity() {

    companion object {
        const val FILE_PROVIDER = "com.example.imager.fileprovider"
    }

    private val imagesViewModel by lazy { viewModelImages() }

    private lateinit var mainImage: ImageView
    private lateinit var progressBar: FrameLayout

    private val store by lazy { Store(applicationContext) }
    private val interactionManager = InteractionManager(this)
    private val imageInteractionFragment by lazy { ImageInteractionFragment.newInstance(interactionManager) }

    private val target = object : Target {
        override fun onPrepareLoad(placeHolderDrawable: Drawable?) {}

        override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) {
            Log.d("onBitmapFailed", "message = ${e?.message}")
        }

        override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
            mainImage.setImageBitmap(bitmap)
            imagesViewModel.currentBitmap = bitmap
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_imager)
        mainImage = findViewById(R.id.main_image)
        progressBar = findViewById(R.id.progress_bar)
        interactionManager.apply {
            lifecycle.addObserver(this)
        }
        DBStore.init(applicationContext)
        init()
    }

    private fun init() {
        if (store.mainImagePath.get().isNotEmpty()) {
            Picasso.get().load(Uri.parse(store.mainImagePath.get())).into(target)
        }
        mainImage.setOnClickListener {
            TakePictureDialogFragment.show(
                supportFragmentManager,
                object : TakePictureDialogFragment.DownloadImageListener {
                    override fun onPrepare() {
                        enableUI(false)
                        enableProgressBar(true)
                    }

                    override fun onError(exception: Exception?) {
                        enableUI(true)
                        enableProgressBar(true)
                        Toast.makeText(this@ImagerActivity, R.string.toast_cant_load_photo, Toast.LENGTH_LONG).show()
                    }

                    override fun onSuccess(bitmap: Bitmap?) {
                        enableUI(true)
                        enableProgressBar(true)
                        bitmap?.let {
                            imagesViewModel.bufferPath = it.createLocalFile(this@ImagerActivity)
                        }
                        imagesViewModel.currentPath.postValue(imagesViewModel.bufferPath!!)
                    }

                })
        }
        supportFragmentManager.beginTransaction()
            .add(R.id.image_list_container, ImagesListFragment.newInstance(), ImagesListFragment.TAG)
            .add(R.id.image_interaction_container, imageInteractionFragment, ImageInteractionFragment.TAG)
            .commit()

        imagesViewModel.currentPath.observe(this, androidx.lifecycle.Observer { uri ->
            store.mainImagePath.set(uri.toString())
            Picasso.get().load(uri).into(target)
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                TakePictureDialogFragment.PICK_GALLERY_REQUEST -> {
                    val uri = data?.data
                    uri?.let { result ->
                        imagesViewModel.currentPath.postValue(saveFile(result))
                    }
                }
                TakePictureDialogFragment.TAKE_PHOTO_FROM_CAMERA_REQUEST -> {
                    imagesViewModel.currentPath.postValue(imagesViewModel.bufferPath!!)
                }
            }
        }
    }

    fun enableProgressBar(isEnabled: Boolean) {
        progressBar.visibility = if (isEnabled) View.VISIBLE else View.GONE
        mainImage.isEnabled = !isEnabled
    }

    fun enableUI(isEnabled: Boolean) {
        mainImage.isEnabled = isEnabled
        imageInteractionFragment.enableUi(isEnabled)
    }
}