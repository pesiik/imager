package com.example.imager

import android.Manifest
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.Toast
import androidx.core.content.FileProvider
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import com.example.imager.viewmodels.viewModelImages
import com.squareup.picasso.MemoryPolicy
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import com.tbruyelle.rxpermissions2.RxPermissions
import io.reactivex.disposables.Disposable
import java.io.File
import java.io.IOException
import java.lang.Exception

class TakePictureDialogFragment(onDownloadListener: DownloadImageListener) :
    DialogFragment() {

    interface DownloadImageListener {
        fun onPrepare()
        fun onError(exception: Exception? = null)
        fun onSuccess(bitmap: Bitmap?)
    }

    private val downloadImageListener = onDownloadListener

    private val target = object : Target {
        override fun onPrepareLoad(placeHolderDrawable: Drawable?) {
            downloadImageListener.onPrepare()
            dialog.dismiss()
        }

        override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) {
            downloadImageListener.onError(e)
        }

        override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
            downloadImageListener.onSuccess(bitmap)
        }

    }

    companion object {
        private const val TAG = "TakePictureDialogFragment"
        const val PICK_GALLERY_REQUEST = 0
        const val TAKE_PHOTO_FROM_CAMERA_REQUEST = 1

        const val PERMISSION_CAMERA = 100
        const val PERMISSION_GALLERY = 101

        fun show(fragmentManager: FragmentManager, onDownloadListener: DownloadImageListener) {
            TakePictureDialogFragment(onDownloadListener).show(fragmentManager, TAG)
        }
    }

    private lateinit var pickFromGalleryButton: Button
    private lateinit var takeFromCameraButton: Button
    private lateinit var takeFromUrlButton: Button
    private lateinit var urlBlock: LinearLayout
    private lateinit var urlEditText: EditText
    private lateinit var submitButton: Button

    private val imagesViewModel by lazy { activity!!.viewModelImages() }

    private var disposable: Disposable? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = LayoutInflater.from(context).inflate(R.layout.fragment_take_picture, null, false)
        pickFromGalleryButton = view.findViewById(R.id.pick_from_gallery)
        takeFromCameraButton = view.findViewById(R.id.take_from_camera)
        takeFromUrlButton = view.findViewById(R.id.take_from_url)
        urlBlock = view.findViewById(R.id.url_block)
        urlEditText = view.findViewById(R.id.url_edit_text)
        submitButton = view.findViewById(R.id.submit_button)

        pickFromGalleryButton.setOnClickListener {
            processPermission(PERMISSION_GALLERY)
        }
        takeFromCameraButton.setOnClickListener {
            processPermission(PERMISSION_CAMERA)
        }
        takeFromUrlButton.setOnClickListener {
            urlBlock.visibility = if (urlBlock.visibility == View.VISIBLE) View.GONE else View.VISIBLE
        }
        submitButton.setOnClickListener {
            getFromUrl()
        }

        return view
    }

    private fun processPermission(permission: Int) {
        with(RxPermissions(this)) {
            disposable = when (permission) {
                PERMISSION_CAMERA -> {
                    request(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        .subscribe { isGranted ->
                            if (isGranted) {
                                Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
                                    takePictureIntent.resolveActivity(activity!!.packageManager)?.also {
                                        createLocalFileAndDismiss(takePictureIntent)
                                    }
                                }
                            } else {
                                dismissWithoutPermissions()
                            }
                        }
                }
                PERMISSION_GALLERY -> {
                    request(Manifest.permission.READ_EXTERNAL_STORAGE)
                        .subscribe { isGranted ->
                            if (isGranted) {
                                val intent =
                                    Intent(Intent.ACTION_GET_CONTENT).apply {
                                        type = "image/*"
                                    }
                                activity?.startActivityForResult(
                                    Intent.createChooser(intent, "Pick image"),
                                    PICK_GALLERY_REQUEST
                                )
                                dialog.dismiss()
                            } else {
                                dismissWithoutPermissions()
                            }
                        }
                }
                else -> null
            }
        }
    }

    private fun dismissWithoutPermissions() {
        Toast.makeText(activity, R.string.toast_need_permission, Toast.LENGTH_LONG).show()
        dialog.dismiss()
    }

    private fun getFromUrl() {
        if (urlEditText.text != null && urlEditText.text.isNotEmpty()) {
            Picasso.get().load(urlEditText.text.toString()).memoryPolicy(MemoryPolicy.NO_CACHE).into(target)
        } else {
            downloadImageListener.onError()
        }
    }

    private fun createLocalFileAndDismiss(takePictureIntent: Intent) {
        val photoFile: File? = try {
            context!!.createImageFile()
        } catch (ex: IOException) {
            null
        }
        photoFile?.also { file ->
            val photoURI: Uri = FileProvider.getUriForFile(
                activity!!,
                ImagerActivity.FILE_PROVIDER,
                file
            )
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
            imagesViewModel.bufferPath = photoURI
            activity?.startActivityForResult(takePictureIntent, TAKE_PHOTO_FROM_CAMERA_REQUEST)
            dialog.dismiss()
        }
    }

    override fun onDismiss(dialog: DialogInterface?) {
        super.onDismiss(dialog)
        if (disposable != null && !disposable!!.isDisposed) {
            disposable?.dispose()
        }
    }
}